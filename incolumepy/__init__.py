from pathlib import Path

root = Path(__file__).resolve().parent
try:
    __path__ += [
        root.joinpath("fractal").as_posix(),
        root.joinpath("fractal", "forms").as_posix(),
    ]
except NameError:
    __path__ = [
        root.joinpath("fractal").as_posix(),
        root.joinpath("fractal", "forms").as_posix(),
    ]

if __name__ == "__main__":
    print(__path__)
